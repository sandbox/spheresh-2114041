(function () {
	jQuery.fn.webform_orders_form_table_updater = function (obj, contant){
		jQuery(document).trigger('brochure_update');
	}
	jQuery.fn.webform_orders_form_table_replace = function (obj, contant){
		jQuery(obj).replaceWith(contant);
		Drupal.attachBehaviors(Drupal.settings);
		jQuery(obj).resetForm();
		jQuery('input', obj).valid();
		jQuery(document).trigger('brochure_update');
	}
	Drupal.behaviors.webform_orders = {
		attach: function (context, settings) {
			jQuery(context).bind('brochure_update', function(){
				var button = jQuery('.webform-client-form', context).find('.brochure-update');
				jQuery(button).trigger('mousedown');
			});

			jQuery('select[name="order"]').change(function (){
				jQuery(this).parents('form').find('input[type="submit"]').trigger('mousedown');
			});
		}
	}
})(jQuery)