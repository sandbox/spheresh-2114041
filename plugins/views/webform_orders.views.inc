<?php
/**
 * @file
 * Module file of Pfizer helper module
 */
/**
 * Use views_data_alter to add items to the node table that are
 * relevant to comments.
 */
function webform_orders_views_data_alter(&$data) {
  $data['node']['brochure_form'] = array(
    'field' => array(
      'title' => t('Brochure form'),
      'help' => t('Display the form'),
      'handler' => 'views_handler_field_brochure_form',
    ),
  );
}

class views_handler_field_brochure_form extends views_handler_field_entity {
  function options_form(&$form, &$form_state) {
    $form['count_of_orders'] = array(
      '#title' => t('Max count'),
      '#type' => 'textfield',
      '#default_value' => !empty($this->options['count_of_orders']) ? $this->options['count_of_orders'] : 10,
    );

    parent::options_form($form, $form_state);

  }
  function option_definition() {
    $options = parent::option_definition();
    $options['count_of_orders'] = array('default' => isset($this->definition['count_of_orders default']) ? $this->definition['count_of_orders default'] : 10, 'bool' => FALSE);
    return $options;
  }



  function render($values) {
    // Build fake $node.
    $node = $this->get_value($values);
    // Only render the links, if they are defined.
    $form =  drupal_get_form('webform_orders_form_' . $node->nid, $node, $this->options['count_of_orders']);

    if(isset($form['order']) && count($form['order']['#options'])) {
      return $form;
    }

    return 'form';
  }
}