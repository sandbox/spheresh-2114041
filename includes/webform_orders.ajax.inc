<?php

/**
 * Implements of hook_webform_component_info().
 */
function webform_orders_webform_component_ajax_alter(&$component) {
	$component['table']['#header'][] = '';
	$delete = array(
		'#type' => 'submit',
		'#value' => t('Delete'),
		'#ajax' => array(
		  'path' => 'propeople/brochure/table/delete',
		)
	);
	foreach ($component['table']['#rows'] as $key => $row) {
		$component['table']['#rows'][$key][1]['data']['#ajax'] = array(
			'callback' => 'webform_orders_table_update_value',
		);
    $component['table']['#rows'][$key][2]['data'] = $delete;
    $component['table']['#rows'][$key][2]['data']['#ajax']['path'] .= '/' . $key;
		$component['table']['#rows'][$key][2]['data']['#name'] = 'op_delete_' . $key;
	}
	$component['update'] = array(
		'#type' => 'submit',
    '#value' => t('Update'),
    '#attributes' => array('class' => array('brochure-update'), /*'style' => 'display:none;'*/),
		'#ajax' => array(
			'callback' => 'webform_orders_table_updater',
			'event' => 'update',
		)
	);
}

function webform_orders_form_brochure_ajax_alter(&$form, $form_state){
	$form['submit']['#ajax'] = array(
		'callback' => 'webform_orders_form_updater',
	);
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'webform_orders') . '/webform_orders.js',
  );
}


function webform_orders_form_updater () {
  list($form, $form_state, $form_id, $form_build_id) = ajax_get_form();
	$id = $form['#id'];
  $form_state['input'] = array();
  $form = drupal_build_form($form_id, $form_state);
  drupal_alter('form',$form, $form_state);

  $commands[] = ajax_command_replace('#' . $id, render($form));


  $commands[] = ajax_command_invoke('document', 'webform_orders_form_table_updater',
                    array($form['#id']));
  print ajax_render($commands); exit;
}


function webform_orders_table_updater () {
  unset($_SESSION['messages']['error']);
  list($form, $form_state, $form_id, $form_build_id) = ajax_get_form();

  $store = &drupal_static('webform_orders');
  $form_state['input'] = array();

  $form = drupal_build_form($form_id, $form_state);
  drupal_alter('form',$form, $form_state);

  $commands[] = ajax_command_invoke('document', 'webform_orders_form_table_replace',
                    array('.webform-component-order_items', render($form['submitted']['orders'])));

  print ajax_render($commands); exit;
}


function webform_orders_table_update_value () {
  list($form, $form_state, $form_id, $form_build_id) = ajax_get_form();

  $store = &drupal_static('webform_orders');
  $form = drupal_build_form($form_id, $form_state);

  foreach ($form_state['values']['submitted']['orders'] as $key => $value) {
  	if(is_numeric($key)){
  		$store->setStore($key, $value);
  	}
  }
  drupal_alter('form',$form, $form_state);

  $commands[] = ajax_command_invoke('document', 'webform_orders_form_table_replace',
                    array('.webform-component-order_items', render($form['submitted']['orders'])));
  $commands[] = ajax_command_replace('.webform-component-order_items', render($form['submitted']['orders']));

  print ajax_render($commands); exit;
}


function webform_orders_table_delete () {
  list($form, $form_state, $form_id, $form_build_id) = ajax_get_form();

  $store = &drupal_static('webform_orders');
  $element_name = $form_state['input']['_triggering_element_name'];
  ;
  if(preg_match('/op\_delete\_([0-9]+)/', $element_name, $match)){
  	$delete_item = substr($element_name, strpos($element_name, 'op_delete_'));
  	$store->setStore($match[1], 0);
  }
  $form_state['input'] = array();

  $form = drupal_build_form($form_id, $form_state);
  drupal_alter('form',$form, $form_state);

  $commands[] = ajax_command_invoke('document', 'webform_orders_form_table_replace',
                    array('.webform-component-order_items', render($form['submitted']['orders'])));
  $commands[] = ajax_command_replace('.webform-component-order_items', render($form['submitted']['orders']));

  print ajax_render($commands); exit;
}
