<?php


function webform_orders_webform_validation_validators() {
  return array(
    'node_reference' => array(
      'name' => "Node reference",
      'component_types' => array(
        'order_items',
      ),
      //need to be  reviewed because of token_field hook disaperence bug of some drupal module
      // 'custom_data' => array(
      //   'label' => t('Specify numeric validation range'),
      //   'description' => theme('token_tree', array('token_types' => 'all')),
      //   'required' => TRUE,
      // ),
      'description' => t('Verifies that user-entered values are numeric, with the option to specify min and / or max values.'),
    ),
  );
}

/**
 * Implements hook_webform_validation_validate().
 *
 */
function webform_orders_webform_validation_validate($validator_name, $items, $components, $rule) {
  if ($items) {
    $errors = array();
    switch ($validator_name) {
      case 'node_reference':
        $max_words = 1;
        $store = &drupal_static('webform_orders');
        foreach ($items as $key => $val) {
          foreach ($val as $nid => $count) {
            if(is_numeric($nid) && !is_numeric($count)){
              $error = t('%item can be nummeric', array('%item' => $components[$key]['name']));
              $errors[$key][$nid] = $error;
            }
          }
        }
        return $errors;
        break;
    }
  }
}

function webform_orders_clientside_validation_webform_alter(&$js_rules, $element, &$context) {
  if ($webform_validation_rules = _clientside_validation_webform_webform_validation($element['values']['details']['nid'], $js_rules)) {
    foreach ($webform_validation_rules as $key => $rules) {
      foreach ($rules['components'] as $component) {
        switch ($rules['validator']) {
          case 'node_reference':
            _webform_orders_webform_set_rules($context, $js_rules['submitted'][$component['form_key']], $rules);
            break;
        }
      }
    }
  }
}
function _webform_orders_webform_set_rules(&$context, &$elements, $rules) {
  $element = array_values(element_children($elements));
  if(!empty($element)){
    foreach ($element as $index => $value) {
      if(isset($elements[$value]['#type']) && $elements[$value]['#type'] == 'textfield'){
        $component = $elements[$value];
        $node =  node_load($component['#nid']);
        $max = $node->field_brochure_quantity['und'][0]['value'];
        $message = t('!name field has to have between !min and !max values..', $val = array(
            '!name' => $component['#title'],
            '!min' => 1,
            '!max' => $max
          )
        );
        _clientside_validation_set_minmax($component['#name'], $component['#title'], $val['!min'], $val['!max'], $context, $message);
      }
      else {
       _webform_orders_webform_set_rules($context, $elements[$value], $rules);
      }
    }
  }
}