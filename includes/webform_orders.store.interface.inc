<?php

class StoreInterface implements Store  {

  private $store_key;

  private static $instance;

  static function getInstance($store_key) {
    if (self::$instance == NULL || self::$instance->getKey() != $store_key) {

      self::$instance = new StoreInterface();
      self::$instance->store_key = $store_key;
      drupal_session_start();


    }

    return self::$instance;
  }

  function getKey() {
    return $this->store_key;
  }

  function setStore($key, $data) {
    if($data){
      $_SESSION[$this->getKey()][$key] = $data;
    }
    else {
      unset($_SESSION[$this->getKey()][$key]);
    }
  }

  function getStore($key) {
    if (isset($_SESSION[$this->getKey()][$key])) {
      return $_SESSION[$this->getKey()][$key];
    }
    return array();
  }
  function getStores() {
    if (isset($_SESSION[$this->getKey()])) {
      return $_SESSION[$this->getKey()];
    }
    return array();
  }

  function clearStore() {
    unset($_SESSION[$this->getKey()]);
  }

}

interface Store {

  /**
   * Instantiation of store object should be handeled through factory
   * to secure correct object initiation. Also there is much doubt you
   * should make it singleton.
   *
   * @param string $store_key
   *   Store key.
   *
   * @return MformsIstore
   *   Instance of MformsIstore.
   */
  static function getInstance($store_key);

  /**
   * Gets store key.
   *
   * @return string
   *   Current store instance key.
   */
  function getKey();

  /**
   * Sets value into store under key.
   *
   * @param string $key
   *   Key under which to store data.
   * @param mixed $data
   *   Data to be stored.
   */
  function setStore($key, $data);

  /**
   * Gets value from store stored under supplied key.
   *
   * @param string $key
   *   Key under which data is stored.
   *
   * @return mixed
   *   Stored data.
   */
  function getStore($key);

  /**
   * Deletes data from store. Each implementation should define a way
   * how to dump data from a store. This is the place to do so.
   */
  function clearStore();

}
