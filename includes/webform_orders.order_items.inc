<?php

/**
 * @file
 * Webform module textfield component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_order_items() {

  return array(
    'name' => '',
    'form_key' => NULL,
    'pid' => 0,
    'weight' => 0,
    'value' => '',
    'mandatory' => 0,
    'extra' => array(
      'width' => '',
      'maxlength' => '',
      'field_prefix' => '',
      'field_suffix' => '',
      'disabled' => 0,
      'unique' => 0,
      'title_display' => 'none',
      'description' => '',
      'attributes' => array(),
      'private' => FALSE,
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_order_items($component) {


  return array();
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_order_items($component, $value = NULL, $filter = TRUE) {

  $node = isset($component['nid']) ? node_load($component['nid']) : NULL;

  $element = array(
    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#name' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'none',
    '#default_value' => $filter ? _webform_filter_values($component['value'], $node, NULL, NULL, FALSE) : $component['value'],
    '#required' => $component['mandatory'],
    '#weight' => $component['weight'],
    '#field_prefix' => empty($component['extra']['field_prefix']) ? NULL : ($filter ? _webform_filter_xss($component['extra']['field_prefix']) : $component['extra']['field_prefix']),
    '#field_suffix' => empty($component['extra']['field_suffix']) ? NULL : ($filter ? _webform_filter_xss($component['extra']['field_suffix']) : $component['extra']['field_suffix']),
    '#description' => $filter ? _webform_filter_descriptions($component['extra']['description'], $node) : $component['extra']['description'],
    '#attributes' => array_merge_recursive( (array) $component['extra']['attributes'], array('class' => array('brochures_order_items'))),
    '#theme_wrappers' => array('webform_element'),
    '#translatable' => array('title', 'description', 'field_prefix', 'field_suffix'),
    '#attached' => array('js' => array(
      drupal_get_path('module', 'webform_orders') . '/webform_orders.js'
    )),
  );
  $element_table = array(

    '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'none',
    '#name' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
    '#type' => 'table',
    '#header' => array(t('Brochure'),t('Count')),
    '#rows' => array(),
  );
  $store = &drupal_static('webform_orders');

  foreach ($value = $store->getStores() as $i => $v) {
    if(!($node = node_load($i))){
      continue;
    }

    $element_table['#rows'][$i] = array(
      $node->title,
      array(
        'data' => array(
          '#type' => 'textfield',
          '#nid' => $i,
          '#title_display' => $component['extra']['title_display'] ? $component['extra']['title_display'] : 'none',
          '#title' => $filter ? _webform_filter_xss($component['name']) : $component['name'],
          '#default_value' => $v,
          '#parents' => array('submitted', $component['form_key']),
          '#disabled' => true
        ),
      )
    );

  }

  $element['table'] = $element_table;
  return $element;
}
/**
 * Implements _webform_display_component().
 */
function _webform_display_order_items($component, $value, $format = 'html') {
  $rows = array();
  foreach ($value as $key => $value) {
    if(!($node = node_load($key))){
      continue;
    }
    $rows[] = array($node->title, $value);
  }
  return array(
    '#type' => 'table',
    '#title' => $component['name'],
    '#weight' => $component['weight'],
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#field_prefix' => $component['extra']['field_prefix'],
    '#field_suffix' => $component['extra']['field_suffix'],
    '#format' => $format,
    '#header' => array(t('Title'),t('Count')),
    '#rows' => $rows,
    '#attributes' => array(),
    '#translatable' => array('title', 'field_prefix', 'field_suffix'),
  );
}


/**
 * Implements _webform_analysis_component().
 */
function _webform_analysis_order_items($component, $sids = array()) {

  $query = db_select('webform_submitted_data', 'wsd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('wsd', array('data'))
    ->condition('nid', $component['nid'])
    ->condition('cid', $component['cid']);

  if (count($sids)) {

    $query->condition('sid', $sids, 'IN');
  }

  $nonblanks = 0;
  $submissions = 0;
  $wordcount = 0;

  $result = $query->execute();
  foreach ($result as $data) {

    if (drupal_strlen(trim($data['data'])) > 0) {

      $nonblanks++;
      $wordcount += str_word_count(trim($data['data']));
    }
    $submissions++;
  }

  $rows[0] = array(t('Left Blank'), ($submissions - $nonblanks));
  $rows[1] = array(t('User entered value'), $nonblanks);
  $rows[2] = array(t('Average submission length in words (ex blanks)'), ($nonblanks != 0 ? number_format($wordcount/$nonblanks, 2) : '0'));
  return $rows;
}

/**
 * Implements _webform_table_component().
 */
function _webform_table_order_items($component, $value) {

  return implode(',', $value);
}

/**
 * Implements _webform_csv_headers_component().
 */
function _webform_csv_headers_order_items($component, $export_options) {

  $header = array();
  $header[0] = '';
  $header[1] = '';
  $header[2] = $component['name'];
  return $header;
}

/**
 * Implements _webform_csv_data_component().
 */
function _webform_csv_data_order_items($component, $export_options, $value) {
    return implode(',', $value);
}
