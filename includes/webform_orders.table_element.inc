<?php

/**
 *
 */
function webform_orders_element_info() {
   $type = array(
    'table' => array (
      '#input' => true,
      '#multiple' => true,
      '#process' => array ('webform_orders_process_element'),
      '#empty' => '',
      '#theme' => 'webform_orders',
      '#type' => 'table',
    )
  );
  return $type;
}

function webform_orders_process_element($element) {
  
  $element['#tree'] = true;
  
  foreach ($element['#rows'] as $row_nummber => $row) {
    
    foreach ($row as $key_name => $field) {
      if(is_array($field) && isset($field['data']['#type'])){
        $element[$row_nummber][$key_name] = $field;
        if(empty($element[$row_nummber][$key_name]['data']['#parents'])){
          $element[$row_nummber][$key_name]['data']['#parents'] = array(
            $element['#name'], 
            $key_name, 
            $row_nummber, 
            'data'
          );
        }
        else {
          $element[$row_nummber][$key_name]['data']['#parents'][] = $row_nummber;
        }
      }
      elseif (is_string($field)) {
        $element[$row_nummber][$key_name] = array('#makeup' => $field);
      }

    }

  }
  return $element;
}
