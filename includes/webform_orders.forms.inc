<?php
/**
 * Implementation of hook_forms().
 */
function webform_orders_forms($form_id, $args) {
  if (strpos($form_id, 'webform_orders_form') !== FALSE) {
    $forms[$form_id] = array(
      'callback' => 'webform_orders_form',
      'callback arguments' => array($form_id),
    );

    return $forms;
  }
}

/**
 * Implementation of hook_form().
 */
function webform_orders_form($form, $form_state, $form_id, $node, $count_of_orders) {

  $form = array();
  $options = array();

  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $options = array();
  for ($i=1; $i < $count_of_orders; $i++) { 
    $options[$i] = format_plural($i, 'One copy', '@count copies');
  }
  $form['order'] = array(
    '#type' => 'select',
    '#title' => t('Order'),
    '#options' => $options/**/,
    '#empty_option' => t('Number'),
  );

  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Import'), 
    '#attributes' => array(
      'class' => array('brochure-add-item'),
      /*'style' => 'display: none'*/
    )
  );

  $form['#submit'] = array('webform_orders_form_submit');


  return $form;
}

function webform_orders_form_submit($form, $form_state) {

  $store = &drupal_static('webform_orders');
  $store->setStore($form_state['values']['nid'], $form_state['values']['order']);
}
/**
 * Implementation of hook_form().
 */
function webform_orders_form_webform_orders_form_alter(&$form, $form_state) {
  $node = node_load($form['nid']['#value']);
	for ($count=1; $count <= (isset($node->field_brochure_quantity[LANGUAGE_NONE][0]['value']) ? $node->field_brochure_quantity[LANGUAGE_NONE][0]['value'] : 0); $count++) { 
		$form['order']['#options'][$count] = $count; 
	}
  webform_orders_form_brochure_ajax_alter($form, $form_state);
	return $form;
}